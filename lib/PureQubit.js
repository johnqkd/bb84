/*function PureQubit (left, right, opts = { rg: Math.random }) {
	this._val = undefined
	this.measured = false
	Object.defineProperty(this, 'val', {
		get: () => {
			if (this._val !== undefined) return this._val
			this.measured = true
			this._val = opts.rg() < .5 ? left : right
			Object.freeze(this)
			return this._val
		},
		set: (x) => x ?? true,
	})
	return Object.preventExtensions(this)
}*/
function* checker (val) {
	yield val
	yield { measured: val }
	return { measured: val }
}

function PureQubit (left, right, opts = { rg: Math.random }) {
	let hidden = undefined
	let gen = undefined
	let initial = {
		get val() {
			if (hidden !== undefined && gen !== undefined) {
				let ngv = gen.next()
				return ngv.done ? (ngv.value ?? { measured: hidden }) : ngv.value
			} else {
				hidden = opts.rg() < .5 ? left : right
				gen = checker(hidden)
				return gen.next().value
			}
		}
	}
	Object.preventExtensions(initial)
	Object.freeze(initial)
	return initial
}

module.exports = PureQubit