const crypto = require('crypto')
const util = require('util')
const PureQubit = require('./PureQubit')
const Particle = require('./particle')

const v0 = 0
const v1 = 1

function Alice () {
	let startlen = 7
	let a = []
	let b =
	for (let i = 0; i < startlen; i++) {
		a.push(Math.random() < .5 ? v0 : v1)
		b.push(Math.random() < .5 ? v0 : v1)
	}
	let tp = {}
	let a_dump = []
	for (let i = 0; i < startlen; i++) {
		let fl = 1 / Math.sqrt(2)
		let qv00 = v0
		let qv10 = v1
		let qv01 = fl * v0 + fl * v1
		let qv11 = fl * v0 - fl * v1
		let id = a[i] * b[i]
		let pp = Particle.spawn({ [`${v0}`]: [ qv00, qv10 ], [`${v1}`]: [ qv01, qv11 ] })
		tp[`${i}`] = { ai: a[i], bi: b[i], i, particle: pp }
		a_dump.push({ id, particle: pp })
	}
	console.log('ATATA', require('util').inspect(tp))
	let b_dump = Bob(a_dump)

	let a_vals = {}
	for (let i = 0; i < a.startlen; i++) {
		let id = a[i] * b[i]
		a_vals[id] = {
			basis: b[i],
			val: tp[id].particle.val(b[i])
		}
	}
	Bob2(a_vals)

	let rest = []
	for (let i in b_dump) {
		for (let j in a_vals) {
			if (i == j && b_dump[i] == a_vals[j].basis) rest.push(a_vals[j].val)
		}
	}
	let key = rest.join('')
	console.log('ALICE GOT KEY: ', key)
}

var b_filtered = {}
var b_dump = {}
function Bob(input) {
	
	input.forEach((x, i, ar) => {
		let basis = Math.random() < .5 ? `${v0}` : `${v1}`
		b_filtered[x.id] = { basis, val: x.particle.val(basis) }
		b_dump[x.id] = basis
	})
	return b_dump
}
function Bob2(a_vals) {
	let rest = []
	for (let i in b_dump) {
		for (let j in a_vals) {
			console.log('OLOLO', require('util').inspect({ a_vals_j: a_vals[j], b_dump_i: b_dump[i], i, j }))
			// && b_dump[i] == a_vals[j].basis) rest.push(a_vals[j].val)
		}
	}
	let key = rest.join('')
	console.log('BOB GOT KEY: ', key)
}

module.exports = Alice