const PureQubit = require('./PureQubit')

const idGenerator = (len = 9) => crypto.randomBytes(len).toString('hex')

function State (qubits) {
	this._val = undefined
	this.measured = false
	this.val = (basis) => {
		Object.defineProperty(this, 'val', {
			get: () => {
				if (this._val !== undefined) return this._val
				this.measured = true
				this._val = qubits[basis]?.pq?.val
				Object.freeze(this)
				return this._val
			}
		})
		return this.val
	}
	return this
}

function spawn (qvals, opts = { rg: Math.random }) {
	let qubits = {}
	for (let basis in qvals) qubits[basis] = new PureQubit(qvals[basis][0], qvals[basis][1], { rg: opts.rg })
	return new PureParticle(qubits)
}

module.exports = {
	PureParticle,
	spawn
}