# BB84

Hi! Here u can see quantum key distribution algo BB84, being implementated in pure Javascript, without any foreign dependencies.

### **BE CAREFUL TO USE UNTIL ANY SECURITY RESEARCH DONE!**
### STILL UNUSABLE FOR QKD PURPOSES!

What's done and works right now:
* total logic of particular qubit, such as:
* initializing it with two distinct values
* measurement destroys excess data irreversibly, which btw enables
* no-cloning theorem compliance

What's under heavy development right now, but still yet to work:
* encoding bitstring [s] into pairs of qubits
* irreversibility of on-basis measurements too
* bits' cherry picking and Alice-Bob data transfer
* shared key generation

What's on roadmap for the upcoming days or week (I hope), or two (more likely):
* quantum data transfer channel
* comparisonss of deviations from addmissible error
* iterative key regeneration, until suitable results of that comparements

### WORK IN PROGRESS!

Please support me on Patreon ([**CLICK**](https://www.patreon.com/qkd)) if you really wish to enjoy the future without of blind believings in prime numbers difficulties, nor in any shit, which is used to avoid the eavesdropping, while being fundamentally insecure, like third-party certificates we all love, or SMS-based 2FA. Choose post-quantum encryption to protect your digital life, and keep calm until the physics laws change!

![patreon.com/qkd](/uploads/0440b16f2fd51ea75ddba984f5c6a2ee/cat.jpg)